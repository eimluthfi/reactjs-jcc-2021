import Routes from "./component/routes";
import Footer from "./component/Footer";
import "antd/dist/antd.css";

const App = () => {
  return (
    <>
      <Routes />
      <Footer />
    </>
  );
};

export default App;
