import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./Home";
import About from "./About";
import MobileList from "./MobileList";
import MobileForm from "./MobileForm";
import MobileSearch from "./MobileSearch";

import { MobileProvider } from "../context/MobileContext";

import Nav from "./navbar";

const Routes = () => {
  return (
    <>
      <Router>
        <MobileProvider>
          <Nav />
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/mobile-list" exact component={MobileList} />
            <Route path="/mobile-form" exact component={MobileForm} />
            <Route path="/mobile-form/edit/:Id" exact component={MobileForm} />
            <Route
              path="/search/:valueOfSearch"
              exact
              component={MobileSearch}
            />
            <Route path="/about" exact component={About} />
          </Switch>
        </MobileProvider>
      </Router>
    </>
  );
};

export default Routes;
