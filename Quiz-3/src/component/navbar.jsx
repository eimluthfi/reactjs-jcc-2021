import { Link } from "react-router-dom";
import { useState } from "react";
import Logo from "../assets/img/logo.png";
import { useHistory } from "react-router-dom";
import { Button } from "antd";

const Navbar = () => {
  let history = useHistory();
  const [cari, setCari] = useState("");

  const handleSearch = (event) => {
    let value = event.target.value;
    setCari(value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    history.push(`/search/${cari}`);
  };

  return (
    <>
      <div className="topnav">
        <a href="">
          <img src={Logo} width="70" />
        </a>
        <Link to="/">Home</Link>
        <Link to="/mobile-list">Mobile App List</Link>
        <Link to="/about">About</Link>
        <form onSubmit={handleSubmit}>
          <input type="text" value={cari} onChange={handleSearch} />
          <Button
            type="primary"
            style={{
              backgroundColor: "green",
              borderColor: "green",
              marginLeft: "10px",
            }}
          >
            Cari
          </Button>
        </form>
      </div>
    </>
  );
};

export default Navbar;
