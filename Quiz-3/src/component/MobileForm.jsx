import React, { useContext, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { MobileContext } from "../context/MobileContext";
import { message } from "antd";

const MobileForm = () => {
  const { input, setInput, currentId, setCurrentId, functions } =
    useContext(MobileContext);
  const { functionSubmit, functionUpdate, fetchById } = functions;
  let { Id } = useParams();
  let history = useHistory();

  useEffect(() => {
    if (Id !== undefined) {
      fetchById(Id);
    }
  }, []);

  const handleChange = (event) => {
    let typeOfValue = event.target.value;
    let name = event.target.name;

    setInput({ ...input, [name]: typeOfValue });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (currentId === null) {
      functionSubmit();
      message.success("Data berhasil ditambahkan");
      history.push("/mobile-list");
    } else {
      functionUpdate(Id);
      message.success("Data berhasil diedit");
      history.push("/mobile-list");
    }

    setInput({
      category: "",
      description: "",
      image_url: "",
      is_android_app: "",
      is_ios_app: "",
      name: "",
      price: "",
      rating: "",
      release_year: "",
      size: "",
    });
    setCurrentId(null);
  };

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Form Daftar Nilai Mahasiswa</h1>
      <br />
      <form
        method="post"
        onSubmit={handleSubmit}
        style={{
          width: "50%",
          border: "1px solid #aaa",
          margin: "auto",
          padding: "50px",
          marginBottom: "20px",
        }}
      >
        <strong style={{ width: "300px", display: "inline-block" }}>
          Category :
        </strong>
        <input
          style={{ float: "right" }}
          onChange={handleChange}
          value={input.category}
          name="category"
          type="text"
          required
        />
        <br />
        <br />
        <strong style={{ width: "300px", display: "inline-block" }}>
          description :
        </strong>
        <textarea
          style={{ float: "right" }}
          onChange={handleChange}
          value={input.description}
          rows="4"
          cols="50"
          name="description"
          type="text"
          required
        />
        <br />
        <br />
        <strong
          style={{ width: "300px", display: "inline-block", marginTop: "20px" }}
        >
          image_url
        </strong>
        <input
          style={{ float: "right", marginTop: "20px" }}
          onChange={handleChange}
          value={input.image_url}
          name="image_url"
          type="text"
          required
        />
        <br />
        <br />
        <strong style={{ width: "300px", display: "inline-block" }}>
          name
        </strong>
        <input
          style={{ float: "right" }}
          onChange={handleChange}
          value={input.name}
          name="name"
          type="text"
          required
        />
        <br />
        <br />
        <strong style={{ width: "300px", display: "inline-block" }}>
          price
        </strong>
        <input
          style={{ float: "right" }}
          onChange={handleChange}
          value={input.price}
          name="price"
          type="number"
          required
        />
        <br />
        <br />
        <strong style={{ width: "300px", display: "inline-block" }}>
          rating
        </strong>
        <input
          style={{ float: "right" }}
          onChange={handleChange}
          value={input.rating}
          name="rating"
          type="number"
          required
        />
        <br />
        <br />
        <strong style={{ width: "300px", display: "inline-block" }}>
          release_year
        </strong>
        <input
          style={{ float: "right" }}
          onChange={handleChange}
          value={input.release_year}
          name="release_year"
          type="number"
          required
        />
        <br />
        <br />
        <strong style={{ width: "300px", display: "inline-block" }}>
          Android
        </strong>
        <input
          style={{ float: "right" }}
          onChange={handleChange}
          value={input.is_android_app}
          name="is_android_app"
          type="checkbox"
        />
        <strong style={{ width: "300px", display: "inline-block" }}>iOS</strong>
        <input
          style={{ float: "right" }}
          onChange={handleChange}
          value={input.is_ios_app}
          name="is_ios_app"
          type="checkbox"
        />
        <strong style={{ width: "300px", display: "inline-block" }}>
          size
        </strong>
        <input
          style={{ float: "right" }}
          onChange={handleChange}
          value={input.size}
          name="size"
          type="number"
          required
        />
        <br />
        <br />
        <input style={{ float: "right" }} type="submit" />
      </form>
    </>
  );
};
export default MobileForm;
