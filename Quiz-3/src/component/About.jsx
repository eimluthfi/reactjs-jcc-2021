const About = () => {
  return (
    <>
      <div className="row">
        <div className="section">
          <h1 style={{ textAlign: "center" }}>
            <b>Data Peserta Jabarcodingcamp-2021 ReactJs</b>
          </h1>

          <ol>
            <li>
              <b>Nama</b>:Muhammad Luthfi
            </li>
            <li>
              <b>Email</b>:Eimluthfi@gmail.com
            </li>
            <li>
              <b>Sistem Operasi yang digunakan</b>:GITLAB
            </li>
            <li>
              <b>Akun GITLAB</b>:@eimluthfi
            </li>
            <li>
              <b>Akun Telegram</b>:Muhammad Luthfi
            </li>
          </ol>
        </div>
      </div>
    </>
  );
};

export default About;
