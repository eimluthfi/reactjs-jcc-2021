import { useEffect, useContext } from "react";

import { Table, Button, message } from "antd";
import { MobileContext } from "../context/MobileContext";

import { useHistory } from "react-router-dom";

const MobileList = () => {
  let history = useHistory();

  const { mobileList, input, setInput, functions } = useContext(MobileContext);

  const {
    fetchData,
    fetchStatus,
    functionDelete,
    functionUpdate,
    setFetchStatus,
  } = functions;

  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = (event) => {
    let idMobile = parseInt(event.currentTarget.value);
    functionDelete(idMobile);
    message.success("Data berhasil dihapus");
  };

  const handleEdit = (event) => {
    let idMobile = parseInt(event.currentTarget.value);
    functionUpdate(idMobile);
    history.push(`/mobile-form/edit/${idMobile}`);
    // setFetchStatus(true);
  };

  const handleCreate = () => {
    history.push("/mobile-form");
    setInput({
      nama: "",
      course: "",
      score: 0,
    });
  };
  const columns = [
    {
      title: "No",
      dataIndex: "no",
      key: "no",
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Category",
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Release Year",
      dataIndex: "release_year",
      key: "release_year",
    },
    {
      title: "Size",
      dataIndex: "size",
      key: "size",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Rating",
      dataIndex: "rating",
      key: "rating",
    },
    {
      title: "Platform",
      dataIndex: "platform",
      key: "platform",
    },
    {
      title: "Action",
      key: "action",
      render: (res, index) => (
        <div key={index}>
          <Button
            type="primary"
            style={{
              backgroundColor: "yellow",
              borderColor: "yellow",
            }}
            value={res.id}
            onClick={handleEdit}
          >
            Edit
          </Button>

          <Button
            type="primary"
            style={{
              backgroundColor: "red",
              borderColor: "red",
              marginLeft: "10px",
            }}
            value={res.id}
            onClick={handleDelete}
          >
            Delete
          </Button>
        </div>
      ),
    },
  ];

  return (
    <>
      <div class="row">
        <div class="section">
          <h1 style={{ textAlign: "center" }}>Mobile Apps List</h1>

          <Table columns={columns} dataSource={mobileList} />

          <Button
            type="primary"
            style={{
              backgroundColor: "green",
              borderColor: "green",
              marginLeft: "10px",
            }}
            onClick={handleCreate}
          >
            Add Form
          </Button>
        </div>
      </div>
    </>
  );
};

export default MobileList;
