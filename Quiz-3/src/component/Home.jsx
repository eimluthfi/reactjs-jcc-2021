import { useEffect, useContext } from "react";
import "../assets/css/style.css";
import { MobileContext } from "../context/MobileContext";

const Home = () => {
  const { mobileList, functions } = useContext(MobileContext);
  const { fetchData } = functions;

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="row">
        <div className="section">
          {mobileList.map((data) => {
            return (
              <div className="card">
                <div>
                  <h2>{data.name}</h2>
                  <h5>Release Year : {data.release_year}</h5>
                  <img
                    src={data.image_url}
                    style={{
                      width: "50%",
                      height: "300px",
                      objectFit: "cover",
                    }}
                  />
                  <br />
                  <br />
                  <div>
                    <strong>Price: {data.price}</strong>
                    <br />
                    <strong>Rating: {data.rating}</strong>
                    <br />
                    <strong>Size: {data.size}</strong>
                    <br />
                    <strong style={{ marginRight: "10px" }}>
                      Platform : Android & IOS
                    </strong>
                    <br />
                  </div>
                  <p>
                    <strong style={{ marginRight: "10px" }}>
                      Description :
                    </strong>
                    {data.description}
                  </p>
                  <hr />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Home;
