import axios from "axios";
import React, { createContext, useState } from "react";
import { useHistory } from "react-router-dom";

export const MobileContext = createContext();

export const MobileProvider = (props) => {
  let history = useHistory();

  const [mobileList, setMobileList] = useState([]);
  const [input, setInput] = useState({
    category: "",
    description: "",
    image_url: "",
    is_android_app: "",
    is_ios_app: "",
    name: "",
    price: "",
    rating: "",
    release_year: "",
    size: "",
  });
  const [search, setSearch] = useState([]);

  const [currentId, setCurrentId] = useState(null);

  const [fetchStatus, setFetchStatus] = useState(false);

  const MB = (value) => {
    if (value < 1000) {
      return value / 100 + " MB";
    } else {
      return value / 1000 + " GB";
    }
  };
  const fetchData = async () => {
    let result = await axios.get(
      `http://backendexample.sanbercloud.com/api/mobile-apps`
    );
    let data = result.data;

    setMobileList(
      data.map((e, index) => {
        let prices = e.price > 0 ? e.price : "FREE";
        let size = MB(e.size);
        return {
          no: index + 1,
          category: e.category,
          created_at: e.created_at,
          description: e.description,
          id: e.id,
          image_url: e.image_url,
          is_android_app: e.is_android_app,
          is_ios_app: e.is_ios_app,
          name: e.name,
          price: prices,
          rating: e.rating,
          release_year: e.release_year,
          size: size,
        };
      })
    );
  };

  const fetchById = async (idMobile) => {
    let res = await axios.get(
      `http://backendexample.sanbercloud.com/api/mobile-apps/${idMobile}`
    );
    let data = res.data;
    setInput({
      category: data.category,
      description: data.description,
      image_url: data.image_url,
      is_android_app: data.is_android_app,
      is_ios_app: data.is_ios_app,
      name: data.name,
      price: data.price,
      rating: data.rating,
      release_year: data.release_year,
      size: data.size,
    });
    setCurrentId(data.id);
  };

  const functionSubmit = () => {
    axios
      .post(`http://backendexample.sanbercloud.com/api/mobile-apps`, {
        category: input.category,
        description: input.description,
        image_url: input.image_url,
        is_android_app: input.is_android_app,
        is_ios_app: input.is_ios_app,
        name: input.name,
        price: input.price,
        rating: input.rating,
        release_year: input.release_year,
        size: input.size,
      })
      .then((res) => {
        let data = res.data;
        setMobileList([
          ...mobileList,
          {
            category: data.category,
            created_at: data.created_at,
            description: data.description,
            id: data.id,
            image_url: data.image_url,
            is_android_app: data.is_android_app,
            is_ios_app: data.is_ios_app,
            name: data.name,
            price: data.price,
            rating: data.rating,
            release_year: data.release_year,
            size: data.size,
          },
        ]);
      });
  };

  const functionUpdate = async () => {
    await axios.put(
      `http://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`,
      {
        category: input.category,
        description: input.description,
        image_url: input.image_url,
        is_android_app: input.is_android_app,
        is_ios_app: input.is_ios_app,
        name: input.name,
        price: input.price,
        rating: input.rating,
        release_year: input.release_year,
        size: input.size,
      }
    );

    let newMobileList = mobileList.find((el) => el.id === currentId);
    newMobileList.category = input.category;
    newMobileList.description = input.description;
    newMobileList.image_url = input.image_url;
    newMobileList.is_android_app = input.is_android_app;
    newMobileList.is_ios_app = input.is_ios_app;
    newMobileList.name = input.name;
    newMobileList.price = input.price;
    newMobileList.rating = input.rating;
    newMobileList.release_year = input.release_year;
    newMobileList.size = input.size;
    setMobileList([...mobileList]);
  };

  const functionDelete = (idMahasiswa) => {
    axios
      .delete(
        `http://backendexample.sanbercloud.com/api/mobile-apps/${idMahasiswa}`
      )
      .then(() => {
        let newDataMobileList = mobileList.filter((res) => {
          return res.id !== idMahasiswa;
        });
        setMobileList(newDataMobileList);
      });
  };

  const functionSearch = async (x) => {
    let result = await axios.get(
      `http://backendexample.sanbercloud.com/api/mobile-apps`
    );
    let data = result.data;
    setSearch(
      data.map((e, index) => {
        return {
          no: index + 1,
          category: e.category,
          created_at: e.created_at,
          description: e.description,
          id: e.id,
          image_url: e.image_url,
          is_android_app: e.is_android_app,
          is_ios_app: e.is_ios_app,
          name: e.name,
          price: e.price,
          rating: e.rating,
          release_year: e.release_year,
          size: e.size,
        };
      })
    );

    // let mobileSearch = search.filter((word) => word.name.toLowerCase() === x);
    // setSearch(mobileSearch);
    console.log(search);
  };

  const functions = {
    fetchData,
    functionSubmit,
    functionUpdate,
    functionDelete,
    functionSearch,
    fetchById,
  };

  return (
    <MobileContext.Provider
      value={{
        mobileList,
        setMobileList,
        input,
        setInput,
        currentId,
        setCurrentId,
        search,
        setSearch,
        functions,
        fetchStatus,
        setFetchStatus,
      }}
    >
      {props.children}
    </MobileContext.Provider>
  );
};
