buatlah CRUD Mobile Apps List yang berisi table Mobile Apps beserta delete dan edit.
( form Mobile Apps Wajib terpisah dari list data, yang diatas hanya contoh )
untuk atribut yang dimiliki oleh Mobile Apps diantaranya adalah:
name(string)
description(textarea)
category(string)
release_year(integer)- defaultnya 2007, minimal tahun 2007 dan maksimal 2021
size(integer) - ini dalam satuan MB
price (integer)
rating (integer) - ini minimal 0, maksimal 5
image_url(string) ( untuk image url ambil "copy image addres " dari google saja ) ( contoh nya ada dibawah sekali nanti )
is_android_app (boolean) - defaultnya true
is_ios_app (boolean) - defaultnya true
semua input required pastikan validasi inputnya disesuaikan dengan atribut diatas (misal release_year minimal di isi dengan 2007)
untuk platform di wajibkan minimal pilih salah satu yang di ceklis

berikut ini url API yang digunakan dalam CRUD Mobile Apps List ini:
    GET http://backendexample.sanbercloud.com/api/mobile-apps

    POST http://backendexample.sanbercloud.com/api/mobile-apps

    GET http://backendexample.sanbercloud.com/api/mobile-apps/{ID_MOBILE_APPS}

    PUT http://backendexample.sanbercloud.com/api/mobile-apps/{ID_MOBILE_APPS}

    DELETE http://backendexample.sanbercloud.com/api/mobile-apps/{ID_MOBILE_APPS}