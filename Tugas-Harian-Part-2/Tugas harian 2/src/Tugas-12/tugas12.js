import { useState, useEffect } from "react";
import axios from "axios";
import "./Tugas12.css";

const Tugas12 = () => {
  const [maba, setMaba] = useState([]);
  const [input, setInput] = useState({
    name: "",
    course: "",
    score: "",
  });
  const [currentId, setCurrentId] = useState(null);

  useEffect(() => {
    const fetch = async () => {
      const hasil = await axios.get(
        "http://backendexample.sanbercloud.com/api/student-scores"
      );

      setMaba(
        hasil.data.map((nilai) => {
          return {
            id: nilai.id,
            name: nilai.name,
            course: nilai.course,
            score: nilai.score,
          };
        })
      );
    };

    fetch();
  }, []);

  const handleName = (event) => {
    let value = event.target.value;
    setInput({ ...input, name: value });
  };
  const handleMataKuliah = (event) => {
    let value = event.target.value;
    setInput({ ...input, course: value });
  };
  const handleNilai = (event) => {
    let value = event.target.value;
    setInput({ ...input, score: value });
  };

  const handleEdit = async (event) => {
    let idSiswa = event.target.value;
    const data = await axios.get(
      `http://backendexample.sanbercloud.com/api/student-scores/${idSiswa}`
    );

    const edit = data.data;

    setInput({
      name: edit.name,
      course: edit.course,
      score: edit.score,
    });
    setCurrentId(edit.id);
  };

  const handleDelete = async (event) => {
    let idSiswa = parseInt(event.target.value);

    await axios.delete(
      `http://backendexample.sanbercloud.com/api/contestants/${idSiswa}`
    );

    let newData = maba.filter((e) => {
      return e.id !== idSiswa;
    });
    setMaba(newData);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (currentId === null) {
      let data = await axios.post(
        `http://backendexample.sanbercloud.com/api/student-scores`,
        {
          name: input.name,
          course: input.course,
          score: input.score,
        }
      );
      const isi = data.data;

      setMaba([
        ...maba,
        {
          id: isi.id,
          name: isi.name,
          course: isi.course,
          score: isi.score,
        },
      ]);
    } else {
      await axios.put(
        `http://backendexample.sanbercloud.com/api/student-scores/${currentId}`,
        {
          name: input.name,
          course: input.course,
          score: input.score,
        }
      );

      let mahasiswa = maba.find((el) => el.id === currentId);
      mahasiswa.name = input.name;
      mahasiswa.course = input.course;
      mahasiswa.score = input.score;
      setMaba([...maba]);
    }

    setInput({
      name: "",
      course: "",
      score: "",
    });
    setCurrentId(null);
  };

  return (
    <div className="maba">
      <h3 className="title">
        <b>Daftar Nilai Mahasiswa</b>
      </h3>
      {maba !== null && (
        <table>
          <th>No</th>
          <th>Nama</th>
          <th>Mata Kuliah</th>
          <th>Nilai</th>
          <th>Indeks Nilai</th>
          <th>Aksi</th>
          {maba.map((item, index) => {
            return (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>{item.course}</td>
                <td>{item.score}</td>
                <td>
                  {item.score >= 80
                    ? "A"
                    : item.score < 80 && item.score >= 70
                    ? "B"
                    : item.score < 70 && item.score >= 60
                    ? "C"
                    : item.score < 60 && item.score >= 50
                    ? "D"
                    : item.score < 50
                    ? "E"
                    : null}
                </td>
                <td>
                  <button onClick={handleEdit} value={item.id}>
                    Edit
                  </button>
                  <button onClick={handleDelete} value={item.id}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </table>
      )}

      <h3 className="title">
        <b>Form Nilai Mahasiswa</b>
      </h3>
      <form onSubmit={handleSubmit}>
        <label>Nama</label>
        <input
          required
          type="text"
          placeholder="Isi Nama"
          value={input.name}
          onChange={handleName}
        />

        <label>Mata Kuliah</label>
        <input
          required
          type="text"
          placeholder="Isi Mata Kuliah"
          value={input.course}
          onChange={handleMataKuliah}
        />
        <label>Nilai</label>
        <input
          required
          min={0}
          max={100}
          type="number"
          placeholder="100"
          value={input.score}
          onChange={handleNilai}
        />
        <button className="button">Submit</button>
      </form>
    </div>
  );
};

export default Tugas12;
