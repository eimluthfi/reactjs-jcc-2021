import React from "react";
import Gambar from "../asset/img/logo.png";
import "./tugas9.css";

const Welcome = (props) => {
  return (
    <section className="text">
      <input className="box" type="checkbox" />
      {props.name}
    </section>
  );
};

const Tugas9 = () => {
  return (
    <>
      <div className="card">
        <div className="image">
          <img src={Gambar} />
        </div>
        <h2 className="title">THINGS TO DO</h2>
        <h6 className="text-title">During Bootcamp in Jabarcodingcamp</h6>
        <Welcome name="Belajar GIT & CLI" />
        <Welcome name="Belajar HTML & CSS" />
        <Welcome name="Belajar Javascript" />
        <Welcome name="Belajar ReactJS Dasar" />
        <Welcome name="Belajar ReactJS Advance" />
        <button className="send">SEND</button>
      </div>
    </>
  );
};

export default Tugas9;
