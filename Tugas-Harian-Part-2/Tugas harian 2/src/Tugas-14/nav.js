import React, { useContext } from "react";
import { Link } from "react-router-dom";
import "./nav.css";
import { ThemeContext } from "../context/themeContext";
import { Button, Switch } from "antd";

const Nav = () => {
  const { theme, setTheme } = useContext(ThemeContext);
  console.log(theme);

  const handleChangeTheme = () => {
    setTheme(theme === "light" ? "dark" : "light");
  };

  return (
    <>
      <nav
        className={theme}
        style={{ display: "flex", justifyContent: "space-between" }}
      >
        <div style={{ display: "flex" }}>
          <li>
            <Link to="/">Tugas9</Link>
          </li>
          <li>
            <Link to="/tugas10">Tugas10</Link>
          </li>
          <li>
            <Link to="/tugas11">Tugas11</Link>
          </li>
          <li>
            <Link to="/tugas12">Tugas12</Link>
          </li>
          <li>
            <Link to="/tugas13">Tugas13</Link>
          </li>
          <li>
            <Link to="/tugas14">Tugas14</Link>
          </li>
          <li>
            <Link to="/tugas15">Tugas15</Link>
          </li>
        </div>

        <div
          style={{ marginTop: "auto", marginBottom: "auto", padding: "0 10px" }}
        >
          <Switch
            onClick={handleChangeTheme}
            size="10"
            checkedChildren={theme}
            unCheckedChildren={theme}
            defaultChecked
          />
        </div>
      </nav>
    </>
  );
};

export default Nav;
