import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { NilaiMahasiswaProvider } from "../context/nilaiMahasiswaContext";
import Tugas10 from "../Tugas-10/tugas10";
import Tugas11 from "../Tugas-11/tugas11";
import Tugas12 from "../Tugas-12/tugas12";
import NilaiMahasiswa from "../Tugas-13/nilaiMahasiswa";
import Tugas9 from "../Tugas-9/tugas9";
import Nav from "./nav";

import NilaiMahasiswaFrom from "./nilaiMahasiswaForm";
import NilaiMahasiswaList from "./nilaiMahasiswaList";

import SwitchTheme from "./swtichTheme";
import { ThemeProvider } from "../context/themeContext";
import Tugas15List from "../Tugas-15/Tugas15list";
import Tugas15From from "../Tugas-15/Tugas15Form";

const Routes = () => {
  return (
    <>
      <Router>
        <NilaiMahasiswaProvider>
          <ThemeProvider>
            <Nav />

            <Switch>
              <Route path="/" exact component={Tugas9} />
              <Route path="/tugas10" exact component={Tugas10} />
              <Route path="/tugas11" exact component={Tugas11} />
              <Route path="/tugas12" exact component={Tugas12} />
              <Route path="/tugas13" exact component={NilaiMahasiswa} />

              <Route path="/tugas14" exact>
                <SwitchTheme />
                <NilaiMahasiswaList />
              </Route>
              <Route path="/tugas14/create">
                <NilaiMahasiswaFrom />
              </Route>
              <Route path="/tugas14/edit/:Value">
                <NilaiMahasiswaFrom />
              </Route>
              <Route path="/tugas15" exact>
                <Tugas15List />
              </Route>
              <Route path="/tugas15/create">
                <Tugas15From />
              </Route>
              <Route path="/tugas15/edit/:Value">
                <Tugas15From />
              </Route>
            </Switch>
          </ThemeProvider>
        </NilaiMahasiswaProvider>
      </Router>
    </>
  );
};

export default Routes;
