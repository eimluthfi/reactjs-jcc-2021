// import axios from "axios";
// import React, { createContext, useState } from "react";

// export const MabaContext = createContext();

// export const MabaProvider = (props) => {
//   const [maba, setMaba] = useState([]);
//   const [input, setInput] = useState({
//     name: "",
//     course: "",
//     score: "",
//   });
//   const [currentId, setCurrentId] = useState(null);

//   let [theme, setTheme] = useState("light");
//   let [themes, setThemes] = useState("Dark Mode");

//   const fetch = async () => {
//     const hasil = await axios.get(
//       "http://backendexample.sanbercloud.com/api/student-scores"
//     );

//     setMaba(
//       hasil.data.map((nilai) => {
//         return {
//           id: nilai.id,
//           name: nilai.name,
//           course: nilai.course,
//           score: nilai.score,
//         };
//       })
//     );
//   };

//   const functionEdit = async (params) => {
//     const data = await axios.get(
//       `http://backendexample.sanbercloud.com/api/student-scores/${params}`
//     );

//     const edit = data.data;

//     setInput({
//       name: edit.name,
//       course: edit.course,
//       score: edit.score,
//     });
//     setCurrentId(edit.id);
//   };

//   const functionDelete = async (params) => {
//     await axios.delete(
//       `http://backendexample.sanbercloud.com/api/contestants/${params}`
//     );

//     let newData = maba.filter((e) => {
//       return e.id !== params;
//     });
//     setMaba(newData);
//   };

//   const functionPost = async () => {
//     let data = await axios.post(
//       `http://backendexample.sanbercloud.com/api/student-scores`,
//       {
//         name: input.name,
//         course: input.course,
//         score: input.score,
//       }
//     );
//     const isi = data.data;

//     setMaba([
//       ...maba,
//       {
//         id: isi.id,
//         name: isi.name,
//         course: isi.course,
//         score: isi.score,
//       },
//     ]);
//   };

//   const functionUpdate = async () => {
//     await axios.put(
//       `http://backendexample.sanbercloud.com/api/student-scores/${currentId}`,
//       {
//         name: input.name,
//         course: input.course,
//         score: input.score,
//       }
//     );

//     let mahasiswa = maba.find((el) => el.id === currentId);
//     mahasiswa.name = input.name;
//     mahasiswa.course = input.course;
//     mahasiswa.score = input.score;
//     setMaba([...maba]);
//   };

//   const functions = {
//     fetch,
//     functionPost,
//     functionUpdate,
//     functionDelete,
//     functionEdit,
//   };

//   return (
//     <MabaContext.Provider
//       value={{
//         maba,
//         setMaba,
//         input,
//         setInput,
//         currentId,
//         setCurrentId,
//         theme,
//         setTheme,
//         themes,
//         setThemes,
//         functions,
//       }}
//     >
//       {props.children}
//     </MabaContext.Provider>
//   );
// };
