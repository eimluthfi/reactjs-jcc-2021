import React, { useState } from "react";

const Tugas11 = () => {
  const [pesertaBuah, setPesertaBuah] = useState([
    { nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
    { nama: "Manggis", hargaTotal: 350000, beratTotal: 10000 },
    { nama: "Nangka", hargaTotal: 90000, beratTotal: 2000 },
    { nama: "Durian", hargaTotal: 400000, beratTotal: 5000 },
    { nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000 },
  ]);

  const [inputNama, setInputNama] = useState("");
  const [inputHarga, setInputHarga] = useState("");
  const [inputBerat, setInputBerat] = useState("");
  const [currentIndex, setCurrentIndex] = useState(-1);
  const [edit, setEdit] = useState(false);

  const handleNama = (event) => {
    setInputNama(event.target.value);
  };
  const handleHarga = (event) => {
    setInputHarga(event.target.value);
  };
  const handleBerat = (event) => {
    setInputBerat(event.target.value);
  };
  const handleEdit = (event) => {
    let index = parseInt(event.target.value);
    let value = pesertaBuah[index];
    setEdit(true);
    setInputNama(value.nama);
    setInputHarga(value.hargaTotal);
    setInputBerat(value.beratTotal);
    setCurrentIndex(event.target.value);
  };
  const handleDelete = (event) => {
    let index = parseInt(event.target.value);
    let deleteData = pesertaBuah[index];
    let data = pesertaBuah.filter((e) => {
      return e !== deleteData;
    });
    setPesertaBuah(data);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (edit) {
      let data = pesertaBuah;

      if (currentIndex === -1) {
        data = [
          ...pesertaBuah,
          {
            nama: inputNama,
            hargaTotal: inputHarga,
            beratTotal: inputBerat,
          },
        ];
      } else {
        data[currentIndex] = {
          nama: inputNama,
          hargaTotal: inputHarga,
          beratTotal: inputBerat,
        };
      }
      setPesertaBuah(data);
      setInputNama("");
      setInputHarga("");
      setInputBerat("");
      setEdit(false);
    } else {
      setPesertaBuah([
        ...pesertaBuah,
        {
          nama: inputNama,
          hargaTotal: inputHarga,
          beratTotal: inputBerat,
        },
      ]);
      setInputNama("");
      setInputHarga("");
      setInputBerat("");
    }
  };

  return (
    <>
      <div className="table">
        <h1>Daftar Harga Buah</h1>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat Total</th>
              <th>Harga per kg</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {pesertaBuah.map((res, index) => {
              return (
                <>
                  <tr>
                    <td>{index + 1}</td>
                    <td>{res.nama}</td>
                    <td>{res.hargaTotal}</td>
                    <td>{res.beratTotal / 1000} kg</td>
                    <td>{res.hargaTotal / (res.beratTotal / 1000)}</td>
                    <td>
                      <button onClick={handleEdit} value={index}>
                        Edit
                      </button>
                      <button onClick={handleDelete} value={index}>
                        Delete
                      </button>
                    </td>
                  </tr>
                </>
              );
            })}
          </tbody>
        </table>

        <div className="form">
          <h1>Form Daftar Harga Buah</h1>
          <form onSubmit={handleSubmit}>
            <label>Nama</label>
            <br />
            <input
              required
              type="text"
              value={inputNama}
              onChange={handleNama}
            />
            <br />
            <label>Harga Total</label>
            <br />
            <input
              required
              type="number"
              value={inputHarga}
              onChange={handleHarga}
            />
            <br />

            <label>Berat Total</label>
            <input
              required
              type="number"
              value={inputBerat}
              onChange={handleBerat}
              min={2000}
            />
            <br />
            <button>Submit</button>
          </form>
        </div>
      </div>
    </>
  );
};
export default Tugas11;
