import { useEffect, useState } from "react";
import "./tugas10.css";

const Tugas10 = () => {
  let [count, setCount] = useState(100);
  let [statement, setStatement] = useState(false);
  let time = new Date();
  let date = time.toLocaleTimeString();

  useEffect(() => {
    const timers = setTimeout(() => {
      setCount(count - 1);
    }, 1000);

    if (count < 0) {
      clearTimeout(timers);
      setStatement(true);
    }
  }, [count]);

  return (
    <>
      {statement ? null : (
        <div className="countdown">
          <div>
            <h2 className="margin-countdown">Now At - {date}</h2>
            <p className="margin-countdown">Countdown : {count}</p>
          </div>
        </div>
      )}
    </>
  );
};

export default Tugas10;
