import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { NilaiMahasiswaContext } from "../context/nilaiMahasiswaContext";
import { Button, Table, message } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

const Tugas15List = () => {
  let history = useHistory();

  const {
    input,
    setInput,
    dataMahasiswa,
    functions,
    fetchStatus,
    setFetchStatus,
  } = useContext(NilaiMahasiswaContext);
  const { fetchData, getScore, functionDelete, functionEdit } = functions;

  useEffect(() => {
    if (fetchStatus === false) {
      fetchData();
      setFetchStatus(true);
    }
  }, []);
  // }, [fetchData, fetchStatus, setFetchStatus]);

  const handleDelete = (event) => {
    let idMahasiswa = parseInt(event.currentTarget.value);

    functionDelete(idMahasiswa);
    message.success("Data berhasil dihapus");
  };

  const handleEdit = (event) => {
    let idMahasiswa = parseInt(event.currentTarget.value);
    history.push(`/tugas15/edit/${idMahasiswa}`);
    setFetchStatus(true);
  };

  const handleCreate = () => {
    history.push("/tugas15/create");
    setInput({
      nama: "",
      course: "",
      score: 0,
    });
  };

  const columns = [
    {
      title: "Nama",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Mata Kuliah",
      dataIndex: "course",
      key: "course",
    },
    {
      title: "Nilai",
      dataIndex: "score",
      key: "score",
    },
    {
      title: "Index Nilai",
      dataIndex: "indexScore",
      key: "indexScore",
    },
    {
      title: "Action",
      key: "action",
      render: (res, index) => (
        <div key={index}>
          <Button
            type="primary"
            style={{
              backgroundColor: "yellow",
              borderColor: "yellow",
            }}
            value={res.id}
            onClick={handleEdit}
            icon={<EditOutlined />}
          />

          <Button
            type="primary"
            style={{
              backgroundColor: "red",
              borderColor: "red",
              marginLeft: "10px",
            }}
            value={res.id}
            onClick={handleDelete}
            icon={<DeleteOutlined />}
          />
        </div>
      ),
    },
  ];

  return (
    <>
      <br />
      <h1 style={{ textAlign: "center" }}>Daftar Nilai Mahasiswa</h1>
      {dataMahasiswa !== null && (
        <Table columns={columns} dataSource={dataMahasiswa} />
      )}
      <Button
        type="primary"
        style={{
          backgroundColor: "green",
          borderColor: "green",
          marginLeft: "10px",
        }}
        onClick={handleCreate}
      >
        Add Form
      </Button>
    </>
  );
};

export default Tugas15List;
