import React, { useState, useEffect } from "react"
import axios from "axios"

const App = () => {
  
  const [data, setData] = useState([])
  const [fetchTrigger, setFetchTrigger] = useState(true)
  const [input, setInput] = useState({
    search: ""
  })
  useEffect(() => {

    const fetchData = async () => {

      let result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps`)
      console.log(result.data)
      setData(result.data.map((e) => {
        let { name } = e
        return {
          name
        }
      }))

    }


    if (fetchTrigger) {
      fetchData()
      setFetchTrigger(false)
    }


  }, [fetchTrigger, setFetchTrigger])

  const handleChange = (event) => {
    setInput({ ...input, [event.target.name]: event.target.value })
  }

  const handleSearch = (event) => {
    event.preventDefault()

    if (input.search !== "") {
      axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps`)
        .then((e) => {
          let res = e.data

          let filterData = res.filter((e) => {
            return Object.values(e).join(" ").toLowerCase().includes(input.search)
          })

          setData([...filterData])
        })
    }else if(input.search === ""){
      setFetchTrigger(true)
    }
  }

  return (
    <>
      <form onSubmit={handleSearch}>
        <input type="text" name="search" value={input.search} onChange={handleChange} />
      </form>
      <div>
        {data.map((e, index) => {
          return (
            <ul key={index}>
              <li>{e.name}</li>
            </ul>
          )
        })}
      </div>
    </>
  )
}

export default App