import { useState, useEffect, useContext } from "react";
import "./Tugas13.css";
import { MabaContext } from "../context/Mabacontext";
import MabaForm from "./Mabaform";

const MabaList = () => {
  const { maba, functions } = useContext(MabaContext);
  const { fetch, functionDelete, functionEdit } = functions;

  useEffect(() => {
    fetch();
  }, []);

  const handleEdit = async (event) => {
    let idSiswa = event.target.value;
    functionEdit(idSiswa);
  };

  const handleDelete = async (event) => {
    let idSiswa = parseInt(event.target.value);
    functionDelete(idSiswa);
  };

  return (
    <body>
      <div className="maba">
        <h3 className="title">Daftar Nilai Mahasiswa</h3>
        {maba !== null && (
          <table>
            <th>No</th>
            <th>Nama</th>
            <th>Mata Kuliah</th>
            <th>Nilai</th>
            <th>Indeks Nilai</th>
            <th>Aksi</th>
            {maba.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.name}</td>
                  <td>{item.course}</td>
                  <td>{item.score}</td>
                  <td>
                    {item.score >= 80
                      ? "A"
                      : item.score < 80 && item.score >= 70
                      ? "B"
                      : item.score < 70 && item.score >= 60
                      ? "C"
                      : item.score < 60 && item.score >= 50
                      ? "D"
                      : item.score < 50
                      ? "E"
                      : null}
                  </td>
                  <td>
                    <button onClick={handleEdit} value={item.id}>
                      Edit
                    </button>
                    <button onClick={handleDelete} value={item.id}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </table>
        )}

        <h3 className="title">Form Nilai Mahasiswa</h3>
        <MabaForm />
      </div>
    </body>
  );
};

export default MabaList;
