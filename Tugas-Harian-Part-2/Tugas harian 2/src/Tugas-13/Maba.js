import React from "react";
import { MabaProvider } from "../context/Mabacontext";
import MabaList from "./Mabalist";

const Maba = () => {
  return (
    <MabaProvider>
      <MabaList />
    </MabaProvider>
  );
};

export default Maba;
