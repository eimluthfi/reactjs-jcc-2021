import React, { useContext } from "react";
import { MabaContext, MabaProvider } from "../context/Mabacontext";

const MabaForm = () => {
  const { input, setInput, currentId, setCurrentId, functions } =
    useContext(MabaContext);

  const { functionPost, functionEdit } = functions;

  const handleName = (event) => {
    let value = event.target.value;
    setInput({ ...input, name: value });
  };
  const handleMataKuliah = (event) => {
    let value = event.target.value;
    setInput({ ...input, course: value });
  };
  const handleNilai = (event) => {
    let value = event.target.value;
    setInput({ ...input, score: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (currentId === null) {
      functionPost();
    } else {
      functionEdit();
    }

    setInput({
      name: "",
      course: "",
      score: "",
    });
    setCurrentId(null);
  };
  return (
    <form onSubmit={handleSubmit}>
      <label>Nama</label>
      <input
        required
        type="text"
        placeholder="Isi Nama"
        value={input.name}
        onChange={handleName}
      />

      <label>Mata Kuliah</label>
      <input
        required
        type="text"
        placeholder="isi Mata Kuliah"
        value={input.course}
        onChange={handleMataKuliah}
      />
      <label>Nilai</label>
      <input
        required
        min={0}
        max={100}
        type="number"
        placeholder="100"
        value={input.score}
        onChange={handleNilai}
      />
      <button className="button">Submit</button>
    </form>
  );
};

export default MabaForm;
