import React, { useContext } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import { UserContext } from "../Context/UserContext";
import { Layout } from "antd";
import HeaderNav from "./HeaderNav";
import Footer from "./FooterLayout";
import Home from "../Pages/Home";

import GamesCRUD from "../Gaming/GamesCRUD";
import DetailGames from "../Gaming/DetailGames";
import GamesList from "../Gaming/GamesList";
import CreateGames from "../Gaming/CreateGames";
import EditGames from "../Gaming/EditGames";

import MovieCRUD from "../Movies/MovieCRUD";
import DetailMovie from "../Movies/DetailMovie";
import MovieList from "../Movies/MovieList";
import CreateMovie from "../Movies/CreateMovie";
import EditMovie from "../Movies/EditMovie";

import LandingPage from "../Pages/LandingPage";
import Login from "../Pages/Login";
import Register from "../Pages/Register";
import ChangePassword from "../Pages/ChangePassword";
import SiderPage from "../Layout/SiderPage";
import Cookies from "js-cookie";

const Routes = () => {
  const [user] = useContext(UserContext);

  const PrivateRoute = ({ ...props }) => {
    if (Cookies.get("token")) {
      return <Route {...props} />;
    } else {
      return <Redirect to="/login" />;
    }
  };

  const LoginRoute = ({ ...props }) =>
    Cookies.get("token") ? <Redirect to="/home" /> : <Route {...props} />;

  return (
    <Layout>
      <Router>
        <HeaderNav />
        <Layout>
          {Cookies.get("token") ? null : (
            <>
              <SiderPage />
            </>
          )}

          <Layout>
            <Switch>
              <Route exact path="/">
                <LandingPage />
              </Route>
              <Route exact path="/home">
                <Home />
              </Route>
              <Route exact path="/detail-movie/:id">
                <DetailMovie />
              </Route>
              <Route exact path="/detail-games/:id">
                <DetailGames />
              </Route>
              <Route exact path="/movie-list">
                <MovieList />
              </Route>
              <Route exact path="/games-list">
                <GamesList />
              </Route>

              <LoginRoute exact path="/login">
                <Login />
              </LoginRoute>
              <LoginRoute exact path="/register">
                <Register />
              </LoginRoute>

              <PrivateRoute exact path="/movie-create">
                <CreateMovie />
              </PrivateRoute>
              <PrivateRoute exact path="/games-create">
                <CreateGames />
              </PrivateRoute>
              <PrivateRoute exact path="/movie-crud">
                <MovieCRUD />
              </PrivateRoute>
              <PrivateRoute exact path="/games-crud">
                <GamesCRUD />
              </PrivateRoute>
              <PrivateRoute exact path="/movie-edit/:id">
                <EditMovie />
              </PrivateRoute>
              <PrivateRoute exact path="/games-edit/:id">
                <EditGames />
              </PrivateRoute>
              <PrivateRoute exact path="/change-password">
                <ChangePassword />
              </PrivateRoute>
            </Switch>
          </Layout>
        </Layout>
        <Footer />
      </Router>
    </Layout>
  );
};

export default Routes;
