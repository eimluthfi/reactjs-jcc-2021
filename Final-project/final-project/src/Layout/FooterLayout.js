import React from "react";
import { Layout } from "antd";

const { Footer } = Layout;

const FooterLayout = () => {
  return (
    <Footer style={{ height: "50px", backgroundColor: "#17181F" }}>
      <h5 style={{ color: "white", textAlign: "center", marginTop: "-5px" }}>
        <strong>JabarCodingCamp ReactJS</strong> &copy; 2021 Created by{" "}
        <strong>Muhamma Luthfi</strong>
      </h5>
    </Footer>
  );
};

export default FooterLayout;
