/// soal 1
console.log("-----soal 1-----");
console.log("jawaban 1 =");
const LuasLingkaran = (x, r) => {
  return x * r * r;
};
const KelilingLingkaran = (x, r) => {
  return 2 * x * r;
};
let x = 22 / 7;
let r = 7;

console.log(LuasLingkaran(x, r));
console.log(KelilingLingkaran(x, r));

/// soal 2
console.log();
console.log("-----soal 2-----");
console.log("jawaban 2 =");
const introduce = (...rest) => {
  let [name, age, gender, hobby] = rest;
  return `${"Pak"} ${name} ${"adalah seorang"} ${hobby} ${"yang berusia"} ${age} ${"tahun"}`;
};
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis");
console.log(perkenalan);

/// soal 3
console.log();
console.log("-----soal 3-----");
console.log("jawaban 3 =");
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName() {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

console.log(newFunction("John", "Doe").firstName);
console.log(newFunction("Richard", "Roe").lastName);
newFunction("William", "Imoh").fullName();

/// soal 4
console.log();
console.log("-----soal 4-----");
console.log("jawaban 4 =");
let phone = {
  name: "Galaxy Note 20",
  brand: "Samsung",
  year: 2020,
  colors: ["Mystic Bronze", "Mystic White", "Mystic Black"],
};
let { brand: phoneBrand, name: phoneName, year, colors } = phone;

let [colorBronze, colorwhite, colorBlack] = colors;

console.log(phoneBrand, phoneName, year, colorBlack, colorBronze);

/// soal 5
console.log();
console.log("-----soal 5-----");
console.log("jawaban 5 =");
let warna = ["biru", "merah", "kuning", "hijau"];

let dataBukuTambahan = {
  penulis: "john doe",
  tahunTerbit: 2020,
};

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul: ["hitam"],
};
buku = {
  ...buku,
  warnaSampul: [...buku.warnaSampul, ...warna],
  ...dataBukuTambahan,
};
console.log(buku);
