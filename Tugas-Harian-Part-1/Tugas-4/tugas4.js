// soal 1
console.log("soal 1")
console.log(" ")
console.log("jawaban soal 1")
console.log("LOPPING PERTAMA")
for(var deret = 2; deret < 21; deret += 2) {
    console.log(  deret + ': I LOVE CODING');
  }
   
  console.log('-------------------------------');
console.log("LOPPING KEDUA")
for(var deret = 20; deret > 0; deret -= 2) {
    console.log(deret + ': I will become a frontend developer ');
  } 
   
  console.log('-------------------------------');

  // soal 2
console.log("soal 2")
console.log(' ')
console.log('jawaban soal 2')
for(let i = 1; i <= 20; i++){
    if(i%3 == 0 && i%2 == 1){
        console.log(i , '- I love coding');
    }else if(i%2 == 1){
        console.log(i , '- Santai')
    }else{
        console.log(i , '- Berkualitas')
    }
}
console.log(' ')
console.log('-------------------------------');

// soal 3
console.log("soal 3")
console.log(' ')
console.log('jawaban soal 3')
var p = '';
for(var j=1; j<=7; j++){
    for(var k=1; k<=j; k++){
        p += '#';
    }
    p +='\n';
}
console.log(p);
console.log('')
console.log('-------------------------------');

// soal 4
console.log("soal 4")
console.log(' ')
console.log('jawaban soal 4')
var kalimat=["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]
console.log('-ubah kalimat diatas')
kalimat.splice(0,3,'saya')
var slug = kalimat.join(" ")
console.log(kalimat)
console.log('-output')
console.log(slug)
console.log(' ')
console.log('')
console.log('-------------------------------');

// soal 5
console.log("soal 5")
console.log(' ')
console.log('jawaban soal 5')


var sayuran = [];

sayuran.push("Kangkung","Bayam","Buncis","Kubis","Timun","Seledri","Tauge");
sayuran.sort();
for(var l =0; l < sayuran.length; l++){
    console.log((l+1) + '.' + sayuran[l]);
}
console.log('-------------------------------');
