// soal 1
//.... jawaban soal 1
console.log("-----soal 1-----")
function LuasPersegiPanjang(p, l) {
    return p * l
}
function KelilingPersegiPanjang(p, l) {
    return 2 * (p + l)
}
function VolumeBalok(p, l, t) {
    return p * l * t
}
var p = 12;
var l = 4;
var t = 8;

var LuasPersegiPanjang = LuasPersegiPanjang(p, l);
var KelilingPersegiPanjang = KelilingPersegiPanjang(p, l);
var VolumeBalok = VolumeBalok(p, l, t);

console.log(LuasPersegiPanjang);
console.log(KelilingPersegiPanjang);
console.log(VolumeBalok);

// soal 2
//.... jawaban soal 2
console.log("-----soal 2-----")
function introduce(name, age, address, hobby){
  var kalimat = "Nama saya" + " " + name + ",umur saya" + " " + age + 
  ",alamat saya" + " " + address + ",dan saya punya hobby yaitu" + " " + hobby
  return kalimat;
}

var name = "John"
var age = "30 tahun"
var address = "Jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan);
console.log(" ")
 
console.log("Nama saya" + " " + name + ",umur saya" + " " + age + 
 ",alamat saya" + " " + address + ",dan saya punya hobby yaitu" + " " + hobby)

// soal 3
//.... jawaban soal 3
console.log("-----soal 3-----")
var arrayDaftarPeserta = [ "jhon Doe", "laki-laki", "baca buku", 1992]
arrayDaftarPeserta ={
    nama : arrayDaftarPeserta[0],
    jeniskelamin : arrayDaftarPeserta[1],
    hobi : arrayDaftarPeserta[2],
    tahunlahir : arrayDaftarPeserta[3]
}
console.log(arrayDaftarPeserta)
// soal 4
//.... jawaban soal 4
console.log("-----soal 4-----")
var HasilBuah = [
    {
      nama: "Nanas",
      warna: "kuning",
      adaBijinya: false,
      harga: 9000,
    },
    {
      nama: "Jeruk",
      warna: "Oranye",
      adaBijinya: true,
      harga: 8000,
    },
    {
      nama: "Semangka",
      warna: "Hijau & Merah",
      adaBijinya: true,
      harga: 10000,
    },
    {
      nama: "Pisang",
      warna: "kuning",
      adaBijinya: false,
      harga: 5000,
    },
  ];
   
  var filterBuah = HasilBuah.filter(function (buah) {
    return !buah.adaBijinya;
  });
   
  console.log(filterBuah);

  // soal 5
//.... jawaban soal 5
console.log("-----soal 5-----")
function tambahDataFilm (param, param2, param3, param4){
    var objekFilm = {
        nama: param, durasi: param2, genre: param3, tahun: param4
    }
    return dataFilm.push(objekFilm)
}

var dataFilm = []

tambahDataFilm("LOTR", "2 jam", "action", "1999")
tambahDataFilm("Avenger", "2 jam", "action", "2019")
tambahDataFilm("spiderman", " 2 jam", "action", "2004")
tambahDataFilm("juon", "2 jam", "horror", "2004")

console.log(dataFilm)