var filterBooksPromise = require('./promise2.js')

listBuku = () => {
    filterBooksPromise(true, 40)
       .then(function (data){
           console.log(data);
       })
       .catch(function (err) {
           console.log(err.message);
       });
};
listBuku();

listBuku = () => {
    filterBooksPromise(false, 250)
       .then(function (data){
           console.log(data);
       })
       .catch(function (error) {
           console.log(error.message);
       });
};
listBuku();

async function dataBooks() {
    try {
        var data = await filterBooksPromise(true, 30)
        console.log(data);
    } catch (item) {
        console.log(item.message)
    }
}
dataBooks()