var readBooksPromise = require('./promise.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
function execute(time, books, index) { 
    if (index < books.length) { 
        readBooksPromise(time, books[index]) 
            .then(function (fulfilled) { 
                 if (fulfilled > 0) { 
                     index += 1 
                     execute(fulfilled, books, index); 
                }
            }) 
            .catch((err) => { 
                // oops, mom don't buy it 
                 console.log(err.message); 
                // output: 'mom is not happy' 
            }); 
    }
}


execute(10000, books, 0) 