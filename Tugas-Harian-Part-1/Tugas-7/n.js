let arr = [5, 1, 2, 31];
let tmp;

function arrAscending(params) {
  return new Promise((resolve, reject) => {
    for (var i = 0; i < arr.length; j++) {
      for (var j = i + 1; j < arr.length; j++) {
        if (arr[i] > arr[j]) {
          tmp = arr[i];
          arr[i] = arr[j];
          arr[j] = tmp;
        }
      }
    }
    resolve(arr);
  });
}

arrAscending(arr).then((e) => {
  console.log(e);
});
console.log(arrAscending);
