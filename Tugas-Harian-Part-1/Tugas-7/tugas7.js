console.log("-----soal 1-----")
console.log()
console.log("-----realse 0----")
class Animal {
    constructor(name){
        this._legs = 4
        this._cold_blooded = false
        this._name = name       
    }
    get name(){
        return this._name
    }
    get cold_blooded(){
        return this._cold_blooded
    }
    get legs(){
        return this._legs
    }
    set legs(x){
        this._legs = x
    }
}
var sheep = new Animal("shaun");

console.log(sheep.name) 
console.log(sheep.legs) 
console.log(sheep.cold_blooded) 
sheep.legs = 3
console.log(sheep.legs)

console.log("-----realse 1----")
class Ape extends Animal {
    constructor(name, cold_blooded, legs) {
        super(name, cold_blooded, legs)
      
    }
    Yell(){
        console.log("Auooo")
    }
}

class Frog extends Animal {
    constructor(name, cold_blooded, legs){
        super(name, cold_blooded, legs)
        
    }
   jump(){
        console.log("hop hop")
    }
}
var sungokong = new Ape("kera sakti");

sungokong.Yell()
sungokong.legs = 2
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)

var kodok = new Frog("buduk")

kodok.jump() 
kodok.legs = 4
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)

///soal 2
console.log()
console.log("-----soal 2----")
class Clock {
    constructor({ template }) {
      this.template = template;
    }
  
    render() {
      let date = new Date();
  
      let hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      let mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      let secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
  }
  var clock = new Clock({template: 'h:m:s'});
  clock.start();  